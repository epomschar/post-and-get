'use strict'

var app = angular.module('PostAndGetApp', [
  'ngAnimate',
  'ngRoute',
  'ngResource',
  'firebase'])
    .config(function($routeProvider){
      $routeProvider
          .when('/', {
            templateUrl: 'views/browse.html',
            controller: 'PostAndGetController'
          })
          .when('/post', {
            templateUrl: 'views/post.html',
            controller: 'PostAndGetController'
          })
          .when('/edit/:postId', {
            templateUrl: 'views/edit.html',
            controller: 'PostAndGetController'
          })
          .when('/browse', {
            templateUrl: 'views/browse.html',
            controller: 'PostAndGetController'
          })
    });